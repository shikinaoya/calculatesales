package jp.alhinc.shiki_naoya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;

public class CalculateSales {
	public static void main(String[] args){

//		ファイル名フィルターの作成
		FilenameFilter filter = new FilenameFilter(){
			public boolean accept(File folder, String earningsname){
                return new File(folder, earningsname).isFile() && earningsname.matches("^\\d{8}.rcd$");
			}
		};

		try {
			BufferedReader br = null;
			BufferedReader brf = null;

//			Mapの作成
			HashMap<String, String> storeList = new HashMap<>();
			HashMap<String, Long> earningsList = new HashMap<>();
			File file = new File(args[0], "branch.lst");
			if(!file.exists()){
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			try{
//				支店定義ファイルの入力
				String line;
				br = new BufferedReader(new FileReader(file));

//					支店定義ファイルを1行ずつ読み込みstoreListへ保持、フォーマットの確認
				while((line = br.readLine()) != null){
					String[] split = line.split(",", -1);
					if (split.length != 2 || !split[0].matches("^\\d{3}$")) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					storeList.put(split[0], split[1]);
					earningsList.put(split[0],  (long) 0);
				}
			} finally {
				br.close();
			}

//				コマンドライン引数のディレクトリにあるファイル、フォルダすべてから、
//				filterでtrueとなるもののみlist配列へ追加
			File[] list = new File(args[0]).listFiles(filter);
//				売上ファイルが連番か確認
			for(int i=0; i<list.length-1; i++){
				String ab = list[i].getName();
				String abc = ab.substring(1,8);
				int abcd = Integer.parseInt(abc);
				String bc = list[i+1].getName();
				String bcd = bc.substring(1,8);
				int bcde = Integer.parseInt(bcd);
				if(abcd + 1 != bcde){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
			try{

//				売上ファイルがあればバッファへ
				if(list != null){
					for(int i=0; i<list.length; i++){
						brf = new BufferedReader(new FileReader(list[i]));

//						エラーメッセージ用にファイル名を取得
						String filename = list[i].getName();
//						1行目をString型変数linefへ入力
						String linef = brf.readLine();
//						2行目をlinefiへ入力
						String linefi = brf.readLine();

//						3行目をnsへ入力し、nullでなければエラーとする
						String ns = brf.readLine();
						if(ns != null){
							System.out.println(filename+"のフォーマットが不正です");
							return;
						}

//						1行目が支店コードとしてstoreListに存在するか確認
						if(earningsList.get(linef) == null){
							System.out.println(filename+"の支店コードが不正です");
							return;
						}

//						1行目をキーにearningsListからlong型変数eraningsへ現在の値を入力
						long earnings = earningsList.get(linef);

//						linefiをlong型へ変更し、eraningsへ加算し代入
						long linefis = Long.parseLong(linefi);
						earnings += linefis;

//						現時点での合計金額桁数の確認
						int earningslen = String.valueOf(earnings).length();
						if(earningslen>10){
							System.out.println("合計金額が10桁を超えました");
							return;
						}

//						合計金額をearningsListへ再度追加
						earningsList.put(linef,earnings);
					}
				}
			} catch(IOException e){
				System.out.println("");
				return;
			} finally {
				brf.close();
			}

//			売上ファイルを集計
			try{
//				集計ファイルを出力
				File filew = new File(args[0], "branch.out");
				BufferedWriter bw = new BufferedWriter(new FileWriter(filew));

//				集計ファイルの内容を書き込み
				for(String str : storeList.keySet()){
					bw.write(str + "," + storeList.get(str)  + "," + earningsList.get(str));
					bw.newLine();
				}
				bw.close();
				} catch(IOException e){
					System.out.println("");
					return;
			}
			} catch (Exception e) {
				System.out.println("予期せぬエラーが発生しました");
			}
	}
}
